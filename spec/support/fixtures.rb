# frozen_string_literal: true

module Fixtures
  def self.text_files
    Dir[File.join(fixtures_path, '**/*.text')]
  end

  def self.html_file(text_file)
    text_file.sub('.text', '.html')
  end

  def self.fixtures_path
    File.expand_path(File.join(File.dirname(__FILE__), '../fixtures'))
  end
end
